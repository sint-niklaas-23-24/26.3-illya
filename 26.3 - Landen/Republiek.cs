﻿namespace _26._3___Landen
{
    internal class Republiek : Land
    {
        //attr
        private string _president;
        //constr
        public Republiek(string naam, string hoofdstad, string president) : base(naam, hoofdstad)
        {
            President = president;
        }
        //proper
        public string President
        {
            get { return _president; }
            set { _president = value; }
        }
        //method
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
