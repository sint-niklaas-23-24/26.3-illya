﻿namespace _26._3___Landen
{
    internal class Monarchie : Land
    {
        //attr
        private string _koning;
        //constr
        public Monarchie(string naam, string hoofdstad, string koning) : base(naam, hoofdstad)
        {
            Koning = koning;
        }
        //proper
        public string Koning
        {
            get { return _koning; }
            set { _koning = value; }
        }
        //method
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
