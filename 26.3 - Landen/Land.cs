﻿namespace _26._3___Landen
{
    internal class Land
    {
        //attr
        private string _hoofdstad;
        private string _naam;
        //constr
        public Land() { }
        public Land(string naam, string hoofdstad)
        {
            Hoofdstad = hoofdstad;
            Naam = naam;
        }
        //propert
        public string Hoofdstad
        {
            get { return _hoofdstad; }
            set { _hoofdstad = value; }
        }
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        //Methode
        public override string ToString()
        {
            return "iets";
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Land g = (Land)obj;
                    if (this.Naam == g.Naam)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
    }
}
